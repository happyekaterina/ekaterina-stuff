# also can use: from numpy.linalg import eig (eig(a))
import sympy as sp

c1, c2, c3, b = sp.symbols('c1, c2, c3, b', real=True, positive=True)
lam = sp.symbols('lambda')  # introducing eigen value
a = sp.Matrix([[b-lam, c2, c3], [c1, b-lam, c3], [c1, c2, b-lam]])  # 3*3 symbol matrix
#cp = sp.det(a - lam * sp.eye(3))  # sp.eye returns a 2-D array with ones on the diagonal and zeros elsewhere
cp = sp.det(a)
eigs = sp.solveset(cp, lam)
print(eigs)
#print(eig(a))