from sympy import symbols, Eq, solve

b1, b2, b3, c, l = symbols('b1 b2 b3 c l')
eq1 = Eq((b1**2 + 2*c**2 - l)*(b2**2 + 2*c**2 - l)*(b3**2 + 2*c**2 - l) - (b1**2 + 2*c**2 - l)*(b2*c + b3*c + c**2)**2 -
         (b2**2 + 2*c**2 - l)*(b1*c + b3*c + c**2)**2 - (b3**2 + 2*c**2 - l)*(b1*c + b2*c + c**2)**2 +
         2*(b1*c + b2*c + c**2)*(b1*c + b3*c + c**2)*(b2*c + b3*c + c**2))
sol = solve(eq1, l)
print(sol)