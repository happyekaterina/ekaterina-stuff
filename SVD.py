import sympy as sp

b1, b2, b3, z = sp.symbols('b1 b2 b3 z')
#b1, b2, b3, c = sp.symbols('b1 b2 b3 c')
a = sp.Matrix([[sp.exp(1j*b1*z), 0, 0], [0, sp.exp(1j*b2*z), 0], [0, 0, sp.exp(1j*b3*z)]])
#a = sp.Matrix([[b1, c, c], [c, b2, c], [c, c, b3]])
b = sp.MatrixBase.singular_values(a)
print(b)